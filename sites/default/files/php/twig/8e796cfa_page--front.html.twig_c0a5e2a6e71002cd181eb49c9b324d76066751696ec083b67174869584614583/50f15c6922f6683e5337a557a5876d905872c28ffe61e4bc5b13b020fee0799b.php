<?php

/* themes/custom/robotic/templates/page--front.html.twig */
class __TwigTemplate_5727d45ac2500e6f60eec24ee0af8a27131b35134e7e20f661ae6a0a8b017910 extends Twig_Template
{
    public function __construct(Twig_Environment $env)
    {
        parent::__construct($env);

        $this->parent = false;

        $this->blocks = array(
            'navbar' => array($this, 'block_navbar'),
            'main' => array($this, 'block_main'),
            'header' => array($this, 'block_header'),
            'highlighted' => array($this, 'block_highlighted'),
            'breadcrumb' => array($this, 'block_breadcrumb'),
            'action_links' => array($this, 'block_action_links'),
            'help' => array($this, 'block_help'),
            'content' => array($this, 'block_content'),
            'sidebar_first' => array($this, 'block_sidebar_first'),
            'process' => array($this, 'block_process'),
            'sidebar_second' => array($this, 'block_sidebar_second'),
            'footer' => array($this, 'block_footer'),
        );
    }

    protected function doDisplay(array $context, array $blocks = array())
    {
        $tags = array("set" => 61, "if" => 63, "block" => 64);
        $filters = array("clean_class" => 69, "t" => 79);
        $functions = array();

        try {
            $this->env->getExtension('sandbox')->checkSecurity(
                array('set', 'if', 'block'),
                array('clean_class', 't'),
                array()
            );
        } catch (Twig_Sandbox_SecurityError $e) {
            $e->setTemplateFile($this->getTemplateName());

            if ($e instanceof Twig_Sandbox_SecurityNotAllowedTagError && isset($tags[$e->getTagName()])) {
                $e->setTemplateLine($tags[$e->getTagName()]);
            } elseif ($e instanceof Twig_Sandbox_SecurityNotAllowedFilterError && isset($filters[$e->getFilterName()])) {
                $e->setTemplateLine($filters[$e->getFilterName()]);
            } elseif ($e instanceof Twig_Sandbox_SecurityNotAllowedFunctionError && isset($functions[$e->getFunctionName()])) {
                $e->setTemplateLine($functions[$e->getFunctionName()]);
            }

            throw $e;
        }

        // line 60
        echo "
";
        // line 61
        $context["container"] = (($this->getAttribute($this->getAttribute((isset($context["theme"]) ? $context["theme"] : null), "settings", array()), "fluid_container", array())) ? ("container-fluid") : ("container"));
        // line 63
        if (($this->getAttribute((isset($context["page"]) ? $context["page"] : null), "navigation", array()) || $this->getAttribute((isset($context["page"]) ? $context["page"] : null), "navigation_collapsible", array()))) {
            // line 64
            echo "  ";
            $this->displayBlock('navbar', $context, $blocks);
        }
        // line 97
        echo "
";
        // line 101
        echo "
";
        // line 103
        $this->displayBlock('main', $context, $blocks);
        // line 205
        echo "
";
        // line 206
        if ($this->getAttribute((isset($context["page"]) ? $context["page"] : null), "footer", array())) {
            // line 207
            echo "  ";
            $this->displayBlock('footer', $context, $blocks);
        }
        // line 217
        echo "

";
    }

    // line 64
    public function block_navbar($context, array $blocks = array())
    {
        // line 65
        echo "    ";
        // line 66
        $context["navbar_classes"] = array(0 => "navbar", 1 => (($this->getAttribute($this->getAttribute(        // line 68
(isset($context["theme"]) ? $context["theme"] : null), "settings", array()), "navbar_inverse", array())) ? ("navbar-inverse") : ("navbar-default")), 2 => (($this->getAttribute($this->getAttribute(        // line 69
(isset($context["theme"]) ? $context["theme"] : null), "settings", array()), "navbar_position", array())) ? (("navbar-" . \Drupal\Component\Utility\Html::getClass($this->getAttribute($this->getAttribute((isset($context["theme"]) ? $context["theme"] : null), "settings", array()), "navbar_position", array())))) : ((isset($context["container"]) ? $context["container"] : null))));
        // line 72
        echo "    <header";
        echo $this->env->getExtension('sandbox')->ensureToStringAllowed($this->env->getExtension('drupal_core')->escapeFilter($this->env, $this->getAttribute((isset($context["navbar_attributes"]) ? $context["navbar_attributes"] : null), "addClass", array(0 => (isset($context["navbar_classes"]) ? $context["navbar_classes"] : null)), "method"), "html", null, true));
        echo " id=\"navbar\" role=\"banner\">
      <div class=\"container\">
        <div class=\"navbar-header\">
          ";
        // line 75
        echo $this->env->getExtension('sandbox')->ensureToStringAllowed($this->env->getExtension('drupal_core')->escapeFilter($this->env, $this->getAttribute((isset($context["page"]) ? $context["page"] : null), "navigation", array()), "html", null, true));
        echo "
          ";
        // line 77
        echo "          ";
        if ($this->getAttribute((isset($context["page"]) ? $context["page"] : null), "navigation_collapsible", array())) {
            // line 78
            echo "            <button type=\"button\" class=\"navbar-toggle\" data-toggle=\"collapse\" data-target=\".navbar-collapse\">
              <span class=\"sr-only\">";
            // line 79
            echo $this->env->getExtension('sandbox')->ensureToStringAllowed($this->env->getExtension('drupal_core')->renderVar(t("Toggle navigation")));
            echo "</span>
              <span class=\"icon-bar\"></span>
              <span class=\"icon-bar\"></span>
              <span class=\"icon-bar\"></span>
            </button>
          ";
        }
        // line 85
        echo "        </div>

        ";
        // line 88
        echo "        ";
        if ($this->getAttribute((isset($context["page"]) ? $context["page"] : null), "navigation_collapsible", array())) {
            // line 89
            echo "          <div class=\"navbar-collapse collapse\">
            ";
            // line 90
            echo $this->env->getExtension('sandbox')->ensureToStringAllowed($this->env->getExtension('drupal_core')->escapeFilter($this->env, $this->getAttribute((isset($context["page"]) ? $context["page"] : null), "navigation_collapsible", array()), "html", null, true));
            echo "
          </div>
        ";
        }
        // line 93
        echo "      </div>
    </header>
  ";
    }

    // line 103
    public function block_main($context, array $blocks = array())
    {
        // line 104
        echo "  <div class=\"home-slider\">
     ";
        // line 106
        echo "      ";
        if ($this->getAttribute((isset($context["page"]) ? $context["page"] : null), "slider", array())) {
            // line 107
            echo "          <div class=\"container-full\" role=\"complementary\">
            ";
            // line 108
            echo $this->env->getExtension('sandbox')->ensureToStringAllowed($this->env->getExtension('drupal_core')->escapeFilter($this->env, $this->getAttribute((isset($context["page"]) ? $context["page"] : null), "slider", array()), "html", null, true));
            echo "
          </div>
      ";
        }
        // line 111
        echo "  </div>
  <div role=\"main\" class=\"main-container container-fluid js-quickedit-main-content\">
    <div class=\"row\">

      ";
        // line 116
        echo "      ";
        if ($this->getAttribute((isset($context["page"]) ? $context["page"] : null), "header", array())) {
            // line 117
            echo "        ";
            $this->displayBlock('header', $context, $blocks);
            // line 122
            echo "      ";
        }
        // line 123
        echo "

      ";
        // line 126
        echo "      ";
        // line 134
        echo "      ";
        // line 135
        $context["content_classes"] = array(0 => "col-sm-12");
        // line 137
        echo "      <section";
        echo $this->env->getExtension('sandbox')->ensureToStringAllowed($this->env->getExtension('drupal_core')->escapeFilter($this->env, $this->getAttribute((isset($context["content_attributes"]) ? $context["content_attributes"] : null), "addClass", array(0 => (isset($context["content_classes"]) ? $context["content_classes"] : null)), "method"), "html", null, true));
        echo ">

        ";
        // line 140
        echo "        ";
        if ($this->getAttribute((isset($context["page"]) ? $context["page"] : null), "highlighted", array())) {
            // line 141
            echo "          ";
            $this->displayBlock('highlighted', $context, $blocks);
            // line 144
            echo "        ";
        }
        // line 145
        echo "
        ";
        // line 147
        echo "        ";
        if ((isset($context["breadcrumb"]) ? $context["breadcrumb"] : null)) {
            // line 148
            echo "          ";
            $this->displayBlock('breadcrumb', $context, $blocks);
            // line 151
            echo "        ";
        }
        // line 152
        echo "
        ";
        // line 154
        echo "        ";
        if ((isset($context["action_links"]) ? $context["action_links"] : null)) {
            // line 155
            echo "          ";
            $this->displayBlock('action_links', $context, $blocks);
            // line 158
            echo "        ";
        }
        // line 159
        echo "
        ";
        // line 161
        echo "        ";
        if ($this->getAttribute((isset($context["page"]) ? $context["page"] : null), "help", array())) {
            // line 162
            echo "          ";
            $this->displayBlock('help', $context, $blocks);
            // line 165
            echo "        ";
        }
        // line 166
        echo "
        ";
        // line 168
        echo "        ";
        $this->displayBlock('content', $context, $blocks);
        // line 172
        echo "      </section>
      
      ";
        // line 175
        echo "      ";
        if ($this->getAttribute((isset($context["page"]) ? $context["page"] : null), "sidebar_first", array())) {
            // line 176
            echo "        ";
            $this->displayBlock('sidebar_first', $context, $blocks);
            // line 181
            echo "      ";
        }
        // line 182
        echo "
      ";
        // line 184
        echo "      ";
        if ($this->getAttribute((isset($context["page"]) ? $context["page"] : null), "process", array())) {
            // line 185
            echo "        ";
            $this->displayBlock('process', $context, $blocks);
            // line 190
            echo "      ";
        }
        // line 191
        echo "
      ";
        // line 193
        echo "      ";
        if ($this->getAttribute((isset($context["page"]) ? $context["page"] : null), "sidebar_second", array())) {
            // line 194
            echo "        ";
            $this->displayBlock('sidebar_second', $context, $blocks);
            // line 199
            echo "      ";
        }
        // line 200
        echo "

    </div>
  </div>
";
    }

    // line 117
    public function block_header($context, array $blocks = array())
    {
        // line 118
        echo "          <div class=\"col-sm-12\" role=\"heading\">
            ";
        // line 119
        echo $this->env->getExtension('sandbox')->ensureToStringAllowed($this->env->getExtension('drupal_core')->escapeFilter($this->env, $this->getAttribute((isset($context["page"]) ? $context["page"] : null), "header", array()), "html", null, true));
        echo "
          </div>
        ";
    }

    // line 141
    public function block_highlighted($context, array $blocks = array())
    {
        // line 142
        echo "            <div class=\"highlighted\">";
        echo $this->env->getExtension('sandbox')->ensureToStringAllowed($this->env->getExtension('drupal_core')->escapeFilter($this->env, $this->getAttribute((isset($context["page"]) ? $context["page"] : null), "highlighted", array()), "html", null, true));
        echo "</div>
          ";
    }

    // line 148
    public function block_breadcrumb($context, array $blocks = array())
    {
        // line 149
        echo "            ";
        echo $this->env->getExtension('sandbox')->ensureToStringAllowed($this->env->getExtension('drupal_core')->escapeFilter($this->env, (isset($context["breadcrumb"]) ? $context["breadcrumb"] : null), "html", null, true));
        echo "
          ";
    }

    // line 155
    public function block_action_links($context, array $blocks = array())
    {
        // line 156
        echo "            <ul class=\"action-links\">";
        echo $this->env->getExtension('sandbox')->ensureToStringAllowed($this->env->getExtension('drupal_core')->escapeFilter($this->env, (isset($context["action_links"]) ? $context["action_links"] : null), "html", null, true));
        echo "</ul>
          ";
    }

    // line 162
    public function block_help($context, array $blocks = array())
    {
        // line 163
        echo "            ";
        echo $this->env->getExtension('sandbox')->ensureToStringAllowed($this->env->getExtension('drupal_core')->escapeFilter($this->env, $this->getAttribute((isset($context["page"]) ? $context["page"] : null), "help", array()), "html", null, true));
        echo "
          ";
    }

    // line 168
    public function block_content($context, array $blocks = array())
    {
        // line 169
        echo "          <a id=\"main-content\"></a>
          ";
        // line 170
        echo $this->env->getExtension('sandbox')->ensureToStringAllowed($this->env->getExtension('drupal_core')->escapeFilter($this->env, $this->getAttribute((isset($context["page"]) ? $context["page"] : null), "content", array()), "html", null, true));
        echo "
        ";
    }

    // line 176
    public function block_sidebar_first($context, array $blocks = array())
    {
        // line 177
        echo "          <section id=\"sidebar-first\" class=\"sidebar_first col-sm-12\" role=\"complementary\">
            ";
        // line 178
        echo $this->env->getExtension('sandbox')->ensureToStringAllowed($this->env->getExtension('drupal_core')->escapeFilter($this->env, $this->getAttribute((isset($context["page"]) ? $context["page"] : null), "sidebar_first", array()), "html", null, true));
        echo "
          </section>
        ";
    }

    // line 185
    public function block_process($context, array $blocks = array())
    {
        // line 186
        echo "          <section id=\"process\" class=\"process col-sm-12\" role=\"complementary\">
            ";
        // line 187
        echo $this->env->getExtension('sandbox')->ensureToStringAllowed($this->env->getExtension('drupal_core')->escapeFilter($this->env, $this->getAttribute((isset($context["page"]) ? $context["page"] : null), "process", array()), "html", null, true));
        echo "
          </section>
        ";
    }

    // line 194
    public function block_sidebar_second($context, array $blocks = array())
    {
        // line 195
        echo "          <section id=\"sidebar-second\" class=\"sidebar_second col-sm-12\" role=\"complementary\">
            ";
        // line 196
        echo $this->env->getExtension('sandbox')->ensureToStringAllowed($this->env->getExtension('drupal_core')->escapeFilter($this->env, $this->getAttribute((isset($context["page"]) ? $context["page"] : null), "sidebar_second", array()), "html", null, true));
        echo "
          </section>
        ";
    }

    // line 207
    public function block_footer($context, array $blocks = array())
    {
        // line 208
        echo "    <footer class=\"footer\" id=\"footer\">
      <div class=\"footer-top\">";
        // line 209
        echo $this->env->getExtension('sandbox')->ensureToStringAllowed($this->env->getExtension('drupal_core')->escapeFilter($this->env, $this->getAttribute((isset($context["page"]) ? $context["page"] : null), "footer_top", array()), "html", null, true));
        echo "</div>
      <div class=\"footer-bottom\">
      <div class=\"";
        // line 211
        echo $this->env->getExtension('sandbox')->ensureToStringAllowed($this->env->getExtension('drupal_core')->escapeFilter($this->env, (isset($context["container"]) ? $context["container"] : null), "html", null, true));
        echo "\" role=\"contentinfo\">
      ";
        // line 212
        echo $this->env->getExtension('sandbox')->ensureToStringAllowed($this->env->getExtension('drupal_core')->escapeFilter($this->env, $this->getAttribute((isset($context["page"]) ? $context["page"] : null), "footer", array()), "html", null, true));
        echo "
      </div></div>
    </footer>
  ";
    }

    public function getTemplateName()
    {
        return "themes/custom/robotic/templates/page--front.html.twig";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  410 => 212,  406 => 211,  401 => 209,  398 => 208,  395 => 207,  388 => 196,  385 => 195,  382 => 194,  375 => 187,  372 => 186,  369 => 185,  362 => 178,  359 => 177,  356 => 176,  350 => 170,  347 => 169,  344 => 168,  337 => 163,  334 => 162,  327 => 156,  324 => 155,  317 => 149,  314 => 148,  307 => 142,  304 => 141,  297 => 119,  294 => 118,  291 => 117,  283 => 200,  280 => 199,  277 => 194,  274 => 193,  271 => 191,  268 => 190,  265 => 185,  262 => 184,  259 => 182,  256 => 181,  253 => 176,  250 => 175,  246 => 172,  243 => 168,  240 => 166,  237 => 165,  234 => 162,  231 => 161,  228 => 159,  225 => 158,  222 => 155,  219 => 154,  216 => 152,  213 => 151,  210 => 148,  207 => 147,  204 => 145,  201 => 144,  198 => 141,  195 => 140,  189 => 137,  187 => 135,  185 => 134,  183 => 126,  179 => 123,  176 => 122,  173 => 117,  170 => 116,  164 => 111,  158 => 108,  155 => 107,  152 => 106,  149 => 104,  146 => 103,  140 => 93,  134 => 90,  131 => 89,  128 => 88,  124 => 85,  115 => 79,  112 => 78,  109 => 77,  105 => 75,  98 => 72,  96 => 69,  95 => 68,  94 => 66,  92 => 65,  89 => 64,  83 => 217,  79 => 207,  77 => 206,  74 => 205,  72 => 103,  69 => 101,  66 => 97,  62 => 64,  60 => 63,  58 => 61,  55 => 60,);
    }
}
/* {#*/
/* /***/
/*  * @file*/
/*  * Default theme implementation to display a single page.*/
/*  **/
/*  * The doctype, html, head and body tags are not in this template. Instead they*/
/*  * can be found in the html.html.twig template in this directory.*/
/*  **/
/*  * Available variables:*/
/*  **/
/*  * General utility variables:*/
/*  * - base_path: The base URL path of the Drupal installation. Will usually be*/
/*  *   "/" unless you have installed Drupal in a sub-directory.*/
/*  * - is_front: A flag indicating if the current page is the front page.*/
/*  * - logged_in: A flag indicating if the user is registered and signed in.*/
/*  * - is_admin: A flag indicating if the user has permission to access*/
/*  *   administration pages.*/
/*  **/
/*  * Site identity:*/
/*  * - front_page: The URL of the front page. Use this instead of base_path when*/
/*  *   linking to the front page. This includes the language domain or prefix.*/
/*  **/
/*  * Navigation:*/
/*  * - breadcrumb: The breadcrumb trail for the current page.*/
/*  **/
/*  * Page content (in order of occurrence in the default page.html.twig):*/
/*  * - title_prefix: Additional output populated by modules, intended to be*/
/*  *   displayed in front of the main title tag that appears in the template.*/
/*  * - title: The page title, for use in the actual content.*/
/*  * - title_suffix: Additional output populated by modules, intended to be*/
/*  *   displayed after the main title tag that appears in the template.*/
/*  * - messages: Status and error messages. Should be displayed prominently.*/
/*  * - tabs: Tabs linking to any sub-pages beneath the current page (e.g., the*/
/*  *   view and edit tabs when displaying a node).*/
/*  * - action_links: Actions local to the page, such as "Add menu" on the menu*/
/*  *   administration interface.*/
/*  * - node: Fully loaded node, if there is an automatically-loaded node*/
/*  *   associated with the page and the node ID is the second argument in the*/
/*  *   page's path (e.g. node/12345 and node/12345/revisions, but not*/
/*  *   comment/reply/12345).*/
/*  **/
/*  * Regions:*/
/*  * - page.header: Items for the header region.*/
/*  * - page.navigation: Items for the navigation region.*/
/*  * - page.navigation_collapsible: Items for the navigation (collapsible) region.*/
/*  * - page.highlighted: Items for the highlighted content region.*/
/*  * - page.help: Dynamic help text, mostly for admin pages.*/
/*  * - page.content: The main content of the current page.*/
/*  * - page.sidebar_first: Items for the first sidebar.*/
/*  * - page.sidebar_second: Items for the second sidebar.*/
/*  * - page.footer: Items for the footer region.*/
/*  **/
/*  * @ingroup templates*/
/*  **/
/*  * @see template_preprocess_page()*/
/*  * @see html.html.twig*/
/*  *//* */
/* #}*/
/* {# dump(_context|keys) #}*/
/* */
/* {% set container = theme.settings.fluid_container ? 'container-fluid' : 'container' %}*/
/* {# Navbar #}*/
/* {% if page.navigation or page.navigation_collapsible %}*/
/*   {% block navbar %}*/
/*     {%*/
/*       set navbar_classes = [*/
/*         'navbar',*/
/*         theme.settings.navbar_inverse ? 'navbar-inverse' : 'navbar-default',*/
/*         theme.settings.navbar_position ? 'navbar-' ~ theme.settings.navbar_position|clean_class : container,*/
/*       ]*/
/*     %}*/
/*     <header{{ navbar_attributes.addClass(navbar_classes) }} id="navbar" role="banner">*/
/*       <div class="container">*/
/*         <div class="navbar-header">*/
/*           {{ page.navigation }}*/
/*           {# .btn-navbar is used as the toggle for collapsed navbar content #}*/
/*           {% if page.navigation_collapsible %}*/
/*             <button type="button" class="navbar-toggle" data-toggle="collapse" data-target=".navbar-collapse">*/
/*               <span class="sr-only">{{ 'Toggle navigation'|t }}</span>*/
/*               <span class="icon-bar"></span>*/
/*               <span class="icon-bar"></span>*/
/*               <span class="icon-bar"></span>*/
/*             </button>*/
/*           {% endif %}*/
/*         </div>*/
/* */
/*         {# Navigation (collapsible) #}*/
/*         {% if page.navigation_collapsible %}*/
/*           <div class="navbar-collapse collapse">*/
/*             {{ page.navigation_collapsible }}*/
/*           </div>*/
/*         {% endif %}*/
/*       </div>*/
/*     </header>*/
/*   {% endblock %}*/
/* {% endif %}*/
/* */
/* {# dump(page.navigation_collapsible.robotic_main_menu) #}*/
/* {# dump(_context|keys) #}*/
/* {# dump(theme) #}*/
/* */
/* {# Main #}*/
/* {% block main %}*/
/*   <div class="home-slider">*/
/*      {# Sidebar First #}*/
/*       {% if page.slider %}*/
/*           <div class="container-full" role="complementary">*/
/*             {{ page.slider }}*/
/*           </div>*/
/*       {% endif %}*/
/*   </div>*/
/*   <div role="main" class="main-container container-fluid js-quickedit-main-content">*/
/*     <div class="row">*/
/* */
/*       {# Header #}*/
/*       {% if page.header %}*/
/*         {% block header %}*/
/*           <div class="col-sm-12" role="heading">*/
/*             {{ page.header }}*/
/*           </div>*/
/*         {% endblock %}*/
/*       {% endif %}*/
/* */
/* */
/*       {# Content #}*/
/*       {#*/
/*         set content_classes = [*/
/*           page.sidebar_first and page.sidebar_second ? 'col-sm-6',*/
/*           page.sidebar_first and page.sidebar_second is empty ? 'col-sm-9',*/
/*           page.sidebar_second and page.sidebar_first is empty ? 'col-sm-9',*/
/*           page.sidebar_first is empty and page.sidebar_second is empty ? 'col-sm-12'*/
/*         ]*/
/*       #}*/
/*       {% */
/*         set content_classes = ['col-sm-12']*/
/*       %}*/
/*       <section{{ content_attributes.addClass(content_classes) }}>*/
/* */
/*         {# Highlighted #}*/
/*         {% if page.highlighted %}*/
/*           {% block highlighted %}*/
/*             <div class="highlighted">{{ page.highlighted }}</div>*/
/*           {% endblock %}*/
/*         {% endif %}*/
/* */
/*         {# Breadcrumbs #}*/
/*         {% if breadcrumb %}*/
/*           {% block breadcrumb %}*/
/*             {{ breadcrumb }}*/
/*           {% endblock %}*/
/*         {% endif %}*/
/* */
/*         {# Action Links #}*/
/*         {% if action_links %}*/
/*           {% block action_links %}*/
/*             <ul class="action-links">{{ action_links }}</ul>*/
/*           {% endblock %}*/
/*         {% endif %}*/
/* */
/*         {# Help #}*/
/*         {% if page.help %}*/
/*           {% block help %}*/
/*             {{ page.help }}*/
/*           {% endblock %}*/
/*         {% endif %}*/
/* */
/*         {# Content #}*/
/*         {% block content %}*/
/*           <a id="main-content"></a>*/
/*           {{ page.content }}*/
/*         {% endblock %}*/
/*       </section>*/
/*       */
/*       {# Sidebar First #}*/
/*       {% if page.sidebar_first %}*/
/*         {% block sidebar_first %}*/
/*           <section id="sidebar-first" class="sidebar_first col-sm-12" role="complementary">*/
/*             {{ page.sidebar_first }}*/
/*           </section>*/
/*         {% endblock %}*/
/*       {% endif %}*/
/* */
/*       {# Sidebar First #}*/
/*       {% if page.process %}*/
/*         {% block process %}*/
/*           <section id="process" class="process col-sm-12" role="complementary">*/
/*             {{ page.process }}*/
/*           </section>*/
/*         {% endblock %}*/
/*       {% endif %}*/
/* */
/*       {# Sidebar Second #}*/
/*       {% if page.sidebar_second %}*/
/*         {% block sidebar_second %}*/
/*           <section id="sidebar-second" class="sidebar_second col-sm-12" role="complementary">*/
/*             {{ page.sidebar_second }}*/
/*           </section>*/
/*         {% endblock %}*/
/*       {% endif %}*/
/* */
/* */
/*     </div>*/
/*   </div>*/
/* {% endblock %}*/
/* */
/* {% if page.footer %}*/
/*   {% block footer %}*/
/*     <footer class="footer" id="footer">*/
/*       <div class="footer-top">{{ page.footer_top }}</div>*/
/*       <div class="footer-bottom">*/
/*       <div class="{{ container }}" role="contentinfo">*/
/*       {{ page.footer }}*/
/*       </div></div>*/
/*     </footer>*/
/*   {% endblock %}*/
/* {% endif %}*/
/* */
/* */
/* */
